package ru.t1.dkozyaikin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.api.repository.IRepository;
import ru.t1.dkozyaikin.tm.api.service.IService;
import ru.t1.dkozyaikin.tm.enumerated.Sort;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.field.IdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozyaikin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        return add(model);
    }

    @Override
    public @NotNull Collection<M> add(@NotNull Collection<M> models) {
        return repository.add(models);
    }

    @Override
    public @NotNull Collection<M> set(@NotNull Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable Integer index) throws AbstractException {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Nullable
    @Override
    public M removeById(@Nullable String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable Integer index) throws AbstractException {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

}
