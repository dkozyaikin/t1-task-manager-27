package ru.t1.dkozyaikin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozyaikin.tm.api.service.IUserOwnedService;
import ru.t1.dkozyaikin.tm.enumerated.Sort;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.field.IdEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozyaikin.tm.exception.field.UserIdEmptyException;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return repository.findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id ==null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize();
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id ==null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    @Nullable
    public M remove(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.remove(userId, model);
    }

}
