package ru.t1.dkozyaikin.tm.api.model;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

import java.io.IOException;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute() throws AbstractException, IOException, ClassNotFoundException;

}
