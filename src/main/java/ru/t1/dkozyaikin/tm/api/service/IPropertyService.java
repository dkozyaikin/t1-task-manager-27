package ru.t1.dkozyaikin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getApplicationVersion();

}
