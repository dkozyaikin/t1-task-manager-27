package ru.t1.dkozyaikin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String passwordHash, @NotNull Role role);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExists(@NotNull String login);

    @NotNull
    Boolean isEmailExists(@NotNull String email);

}
