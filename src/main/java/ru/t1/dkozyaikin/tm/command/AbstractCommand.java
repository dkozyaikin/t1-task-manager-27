package ru.t1.dkozyaikin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.api.model.ICommand;
import ru.t1.dkozyaikin.tm.api.service.IAuthService;
import ru.t1.dkozyaikin.tm.api.service.IServiceLocator;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

import java.io.IOException;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute() throws AbstractException, IOException, ClassNotFoundException;

    @Nullable
    @Override
    public abstract String getName();

    @Nullable
    @Override
    public abstract String getArgument();

    @Nullable
    @Override
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && ! name.isEmpty()) result += name + " : ";
        if (argument != null && ! argument.isEmpty()) result += argument + " : ";
        if (description != null && ! description.isEmpty()) result += description;
        return result;
    }

}
