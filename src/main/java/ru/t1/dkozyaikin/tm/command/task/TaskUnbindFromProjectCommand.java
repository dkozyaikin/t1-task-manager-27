package ru.t1.dkozyaikin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME ="task-unbind-from-project";

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getAuthService().getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, taskId, projectId);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
