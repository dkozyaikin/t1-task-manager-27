package ru.t1.dkozyaikin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show information about application developer";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getPropertyService().getAuthorName());
        System.out.println("Email: " + getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
