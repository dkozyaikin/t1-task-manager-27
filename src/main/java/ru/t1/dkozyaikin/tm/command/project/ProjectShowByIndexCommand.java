package ru.t1.dkozyaikin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME ="project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Find project by index";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getAuthService().getUserId();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
