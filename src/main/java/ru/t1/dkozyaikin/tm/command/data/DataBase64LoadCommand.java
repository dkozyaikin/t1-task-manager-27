package ru.t1.dkozyaikin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.dto.Domain;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import java.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    public static final String NAME = "data-base64-load";

    public static final String DESCRIPTION = "Load from base64 file";

    @Override
    public void execute() throws AbstractException, IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @Nullable
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getDescription() {
        return DESCRIPTION;
    }

}