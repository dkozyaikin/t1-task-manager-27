package ru.t1.dkozyaikin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.dkozyaikin.tm.api.repository.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.repository.IUserRepository;
import ru.t1.dkozyaikin.tm.api.service.*;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.command.data.AbstractDataCommand;
import ru.t1.dkozyaikin.tm.command.data.DataBase64LoadCommand;
import ru.t1.dkozyaikin.tm.command.data.DataBinaryLoadCommand;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozyaikin.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.repository.CommandRepository;
import ru.t1.dkozyaikin.tm.repository.ProjectRepository;
import ru.t1.dkozyaikin.tm.repository.TaskRepository;
import ru.t1.dkozyaikin.tm.repository.UserRepository;
import ru.t1.dkozyaikin.tm.service.*;
import ru.t1.dkozyaikin.tm.util.SystemUtil;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class  Boostrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.dkozyaikin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, propertyService, taskRepository, projectRepository);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(propertyService, userService);

    {
        try {
            @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
            @NotNull final Set<Class<? extends AbstractCommand>> classes =
                    reflections.getSubTypesOf(AbstractCommand.class);
            for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
        } catch (@NotNull AbstractException e) {
            getLoggerService().error(e);
        }

    }

    private static void exit() {
        System.exit(0);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        userService.create("test", "test", "test@mail.ru");
        userService.create("test2", "test2", "test2@mail.ru");
        @NotNull String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
        for (int i = 0; i < 10; i++) {
            taskRepository.add(adminId, new Task(i + " TASK", "T" + i + "D"));
            projectRepository.add(adminId, new Project(i + " PROJECT", "P" + i + "D"));
        }
    }

    @SneakyThrows
    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) processCommand(DataBinaryLoadCommand.NAME);
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME);
    }

    private void initLogger() {
        loggerService.info("Task Manager started");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("Task Manager closed")));
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) exit();
        initPid();
        initDemoData();
        initLogger();
        //initData();
        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[DONE]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(@NotNull final String command) throws AbstractException, IOException, ClassNotFoundException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) throws AbstractException, IOException, ClassNotFoundException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private boolean processArgument(@Nullable String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
