package ru.t1.dkozyaikin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.enumerated.Role;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    public User(@NotNull final String login, @NotNull final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    @NotNull
    public Boolean isLocked() {
        return locked;
    }

    @NotNull
    @Override
    public String toString() {
        return login;
    }

}
